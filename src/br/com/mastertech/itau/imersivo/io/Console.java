package br.com.mastertech.itau.imersivo.io;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Console {

	public void imprimirCabecalho() {
		Impressora.imprimirLn("Olá! bem vindo ao calculador de área 3 mil!");
		Impressora.imprimirLn("Basta informar a medida de cada lado que eu te digo a área :)");
		Impressora.imprimirLn("Vamos começar!\n");
		Impressora.imprimirLn("Obs: digite -1 se quiser parar de cadastrar lados!\n");
	}

	public List<Double> obterLados() {

		Scanner scanner = new Scanner(System.in);

		imprimirCabecalho();
		List<Double> lados = new ArrayList<>();
		boolean deveAdicionarNovoLado = true;

		while (deveAdicionarNovoLado) {
			Impressora.imprimirLn("Informe o tamanho do lado: " + (lados.size() + 1));

			double tamanhoLado = Double.parseDouble(scanner.nextLine());

			if (tamanhoLado <= 0) {
				deveAdicionarNovoLado = false;
			} else {
				lados.add(tamanhoLado);
			}
		}
		return lados;
	}

}
