package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;

import br.com.mastertech.itau.imersivo.io.Console;
import br.com.mastertech.itau.imersivo.io.Impressora;

public class Controlador {

	static List<Double> lados = new ArrayList<>();
	static Forma forma = null;

	public static void tratarArea() {
		Console console = new Console();

		lados = console.obterLados();

		Impressora.imprimirLn("\nLados cadastrados!");
		Impressora.imprimirLn("Agora vamos calcular a área...\n");

		calcularArea();
	}

	public static void tratarFormaInvalida() {
		Impressora.imprimirLn("Forma inválida!");
	}

	public static void tratarCirculo() {
		Impressora.imprimirLn("Eu identifiquei um circulo!");
		forma = new Circulo(lados);
		Impressora.imprimirLn("A área do circulo é " + (forma.calcularArea()));

	}

	public static void tratarQuadradoRetangulo() {
		Impressora.imprimirLn("Eu identifiquei um quadrado/retangulo!");
		forma = new Quadrado(lados);
		Impressora.imprimirLn("A área do quadrado/retangulo é " + (forma.calcularArea()));

	}

	public static void tratarTriangulo() {
		Impressora.imprimirLn("Eu identifiquei um triangulo!");
		forma = new Triangulo(lados);
		double ladoA = lados.get(0);
		double ladoB = lados.get(1);
		double ladoC = lados.get(2);
		if ((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA) {
			Impressora.imprimirLn("A área do triangulo é " + (forma.calcularArea()));
		} else {
			Impressora.imprimirLn("Mas o triangulo informado era inválido :/");
		}
	}

	public static void calcularArea() {
		switch (lados.size()) {
		case 0:
			tratarFormaInvalida(); break;

		case 1:
			tratarCirculo(); break;

		case 2:
			tratarQuadradoRetangulo(); break;
			
		case 3:
			tratarTriangulo(); break;

		default:
			Impressora.imprimirLn("Ops! Eu não conheço essa forma geometrica ¯\\_(⊙_ʖ⊙)_/¯");

		}
	}

}
