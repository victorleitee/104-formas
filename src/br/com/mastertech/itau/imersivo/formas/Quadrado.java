package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public class Quadrado extends Forma {

	public Quadrado(List<Double> lados) {
		super(lados);

	}

	public double calcularArea() {
		return (double) (lados.get(0) * lados.get(0));
	}

}
