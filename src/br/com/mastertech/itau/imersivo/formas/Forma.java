package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;

public abstract class Forma {
	protected List<Double> lados = new ArrayList<>();

	public Forma(List<Double> lados) {
		this.lados = lados;
	}

	public abstract double calcularArea();

}
