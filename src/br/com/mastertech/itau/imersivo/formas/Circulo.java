package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public class Circulo extends Forma {

	public Circulo(List<Double> lados) {
		super(lados);
	}

	public double calcularArea() {
		return (double) (Math.PI * Math.pow(lados.get(0), 2));
	}

}
