package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public class Triangulo extends Forma {

	public Triangulo(List<Double> lados) {
		super(lados);

	}

	public double calcularArea() {
		double s = (lados.get(0) + lados.get(1) + lados.get(2)) / 2;
		return Math.sqrt(s * (s - lados.get(0)) * (s - lados.get(1)) * (s - lados.get(2)));
	}

}
